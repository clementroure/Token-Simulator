import random
from typing import List

from enforce_typing import enforce_types

from engine import AgentBase
from util import globaltokens
from util.base18 import toBase18
from util import constants

from util.constants import BROWNIE_PROJECT080

@enforce_types
class ModifierCounterAgent(AgentBase.AgentBaseNoEvm):

    def __init__(self, name: str, USD: float, OCEAN: float, address: str, getCounter, setCounter):
        super().__init__(name, USD, OCEAN)
        # parameters that are not in Agent based have to be declared after:
        self.address: str = address
        self.getCounter = getCounter
        self.setCounter = setCounter

    def takeStep(self, state):
        rnd = random.randint(0, 1)
        if rnd == 0:
           self._increase(state)
        else:
            self._decrease(state)

    def _increase(self, state):
        counterContract = BROWNIE_PROJECT080.Counter.at(self.address)
        counterContract.incrementCounter()

        _count = counterContract.getCount()

        self.setCounter(_count)

        #test
        

    def _decrease(self, state):
        counterContract = BROWNIE_PROJECT080.Counter.at(self.address)
        counterContract.decrementCounter()

        _count = counterContract.getCount()

        self.setCounter(_count)
        