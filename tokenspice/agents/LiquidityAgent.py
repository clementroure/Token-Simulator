import random
from typing import List

from enforce_typing import enforce_types

from engine import AgentBase
from util import globaltokens
from util.base18 import toBase18
from util import constants

from util.constants import BROWNIE_PROJECT080

import json
from brownie import accounts, Contract
from brownie import web3

@enforce_types
class LiquidityAgent(AgentBase.AgentBaseEvm):

    def __init__(self, name: str, getLiquidityPool, setLiquidityPool, uniswapV2Router, tokenA, tokenB, lpToken, normal_distribution, binomial_distribution, poisson_distribution):
        super().__init__(name)
        # parameters that are not in Agent based have to be declared after:
        self.id = int(name[-1])
        self.getLiquidityPool = getLiquidityPool
        self.setLiquidityPool= setLiquidityPool
        self.uniswapV2Router = uniswapV2Router
        self.tokenA = tokenA
        self.tokenB = tokenB
        self.lpToken = lpToken
        # normal distribution amount
        self.normal_distribution = normal_distribution
        self.binomial_distribution = binomial_distribution
        self.poisson_distribution = poisson_distribution
        self.i = 0 # day
        self.to = self._wallet.account.address
        # transfer tokens on initialization
        unlocked_metamask = accounts.at('0x7bBfecDCF7d0E7e5aA5fffA4593c26571824CB87', force=True)
        unlocked_metamask.transfer(self._wallet.account.address, "0.01 ether") # send eth to the agent
        # send token to the agent
        tokenA.transfer(self._wallet.account.address, 0.003*10**18, {'from': unlocked_metamask}) 
        tokenB.transfer(self._wallet.account.address, 0.001*10**18, {'from': unlocked_metamask}) 
        # start now
        if(self.binomial_distribution[self.i] == 1):
            self._addLiquidity()
        elif(self.lpToken.balanceOf(self.to) > 0):
            self._removeLiquidity()

    def takeStep(self, state):
        self.i = self.i + 1

        if(self.binomial_distribution[self.i] == 1):
            self._addLiquidity()
        elif(self.lpToken.balanceOf(self.to) > 0):
            self._removeLiquidity()
    
    def _removeLiquidity(self):
        # The minimum amount of tokenA that must be received for the transaction not to revert.
        amountAMin = 1
        # The minimum amount of tokenB that must be received for the transaction not to revert.
        amountBMin = 1
        
        current_block = web3.eth.get_block('latest')
        current_timestamp = current_block['timestamp']
        deadline = current_timestamp + 10

        # get amount of tokens in liquidity pool
        tokenA_balance = self.tokenA.balanceOf(self.lpToken.address)
        tokenB_balance = self.tokenB.balanceOf(self.lpToken.address)
        # print()
        # print('- UNISWAP POOL -')
        # print('UNI balance: ', tokenA_balance / 10**18)
        # print('WETH balance: ', tokenB_balance / 10**18)
        # print()
        # 
        # LpToken balance: addliquidity a token and b token = x lptoken. So, multiply by c, to get a/b token addded = a/b total balance. Multiply x lptoken by c to get the lptoken total supply
        # c =  tokenA_balance / tokenA amount addded
        # totalLPsupply = lpToken added * c

        # check liquidity amount (agent balances)
        # 
        # print('- AGENT WALLET -')
        # print('UNI: ', tokenA.balanceOf(to) / 10**18)
        # print('WETH: ', tokenB.balanceOf(to) / 10**18)
        liquidity = self.lpToken.balanceOf(self.to)
        # print("LP balance: ", liquidity / 10**18)
        # print()

        # Allowance -> uniswap can take agent's liquidity tokens
        self.lpToken.approve(self.uniswapV2Router.address, liquidity, {'from': self.to})
        
        tx = self.uniswapV2Router.removeLiquidity(self.tokenA, self.tokenB, liquidity, amountAMin, amountBMin, self.to, deadline, {"from": self.to})   
        # tx.wait(1)

        with open('outdir_csv/logs.txt', 'a') as f:
            f.write('Remove liquidity ' +  self.name +  ' tokenA: ' + str(tokenA_balance) + ' ' + 'tokenB: ' + str(tokenB_balance) + '\n')

    def _addLiquidity(self):
       # ratio tokenA/b value
        tokenA_balance = self.tokenA.balanceOf(self.lpToken.address)
        tokenB_balance = self.tokenB.balanceOf(self.lpToken.address)
        _max = max(tokenA_balance, tokenB_balance)
        _min = min(tokenA_balance, tokenB_balance)
        ratio = _max / _min
        # constant value
        c = 0.0000001*10**18
        # The amount of tokenA to add as liquidity if the B/A price is <= amountBDesired/amountADesired (A depreciates).
        amountADesired = c * self.normal_distribution[self.i] * ratio
        # The amount of tokenB to add as liquidity if the A/B price is <= amountADesired/amountBDesired (B depreciates).
        amountBDesired = c * self.normal_distribution[self.i]

        # Bounds the extent to which the B/A price can go up before the transaction reverts. Must be <= amountADesired.
        amountAMin = 1
        # Bounds the extent to which the A/B price can go up before the transaction reverts. Must be <= amountBDesired.
        amountBMin = 1

        # Unix timestamp after which the transaction will revert
        current_block = web3.eth.get_block('latest')
        current_timestamp = current_block['timestamp']
        deadline = current_timestamp + 3

        # Allowance -> uniswap can take agent's tokens
        self.tokenA.approve(self.uniswapV2Router.address, amountADesired, {'from': self.to})
        self.tokenB.approve(self.uniswapV2Router.address, amountBDesired, {'from': self.to})

        # WARNING: use .call() to get teh returned value !   # remove .call() and put again if cant mint liquidity
        tx = self.uniswapV2Router.addLiquidity(self.tokenA, self.tokenB, amountADesired, amountBDesired, amountAMin, amountBMin, self.to, deadline, {"from": self.to}) 
        # print(tx.txid)

        # Wait for the transaction to be confirmed
        tx_receipt = web3.eth.wait_for_transaction_receipt(tx.txid)

        # Get the return value from the transaction receipt
        returned_value = tx_receipt['contractAddress']

        # # Print the returned value
        # print(returned_value)
        
        # returned_value = contract.addLiquidity.call(tokenA, tokenB, amountADesired, amountBDesired, amountAMin, amountBMin, to, deadline, {"from": to}) 
        # print('amountA:', returned_value[0], 'amountB:', returned_value[1], 'Liquidity:', returned_value[2])

        with open('outdir_csv/logs.txt', 'a') as f:
            f.write('Add liquidity ' +  self.name +  ' tokenA: ' + str(tokenA_balance) + ' ' + 'tokenB: ' + str(tokenB_balance) + '\n')
   