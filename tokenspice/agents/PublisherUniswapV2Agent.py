import random
from typing import List

from enforce_typing import enforce_types

from engine import AgentBase
from util import globaltokens
from util.base18 import toBase18
from util import constants

from util.constants import BROWNIE_PROJECT080

from agents import SwapAgent, LiquidityAgent

import json
from brownie import accounts, Contract
from brownie import web3
import numpy as np

# Uniswap Router
with open("abi/UniswapV2Router02.json") as file:
    abi = json.load(file)
uniswapV2Router_address = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D"
uniswapV2Router = Contract.from_abi("UniswapV2Router", uniswapV2Router_address, abi)

# Uniswap Factory
with open("abi/UniswapV2Factory.json") as file:
    abi = json.load(file)
uniswapV2Factory_address = "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f"
uniswapV2Factory = Contract.from_abi("UniswapV2Factory", uniswapV2Factory_address, abi)

# TokenA
with open("abi/UNI.json") as file:
    abi = json.load(file)
tokenA_address = "0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984"
tokenA = Contract.from_abi("UNI", tokenA_address, abi)

# TokenB
with open("abi/WETH.json") as file:
    abi = json.load(file)
tokenB_address = "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6"
tokenB = Contract.from_abi("WETH", tokenB_address, abi)

@enforce_types
class PublisherUniswapV2Agent(AgentBase.AgentBaseNoEvm):

    def __init__(self, name: str, maxTime):
        super().__init__(name)
        self.maxTime = maxTime

        # Lptoken address
        lpToken_address = uniswapV2Factory.getPair.call(tokenA.address, tokenB.address)

        # Pair
        with open("abi/LPToken.json") as file:
            erc20_abi = json.load(file)
        self.lpToken = Contract.from_abi("LP Token", address= lpToken_address, abi=erc20_abi)

        # liquidity pool at t=0
        tokenA_balance = tokenA.balanceOf(self.lpToken.address)
        tokenB_balance = tokenB.balanceOf(self.lpToken.address)

        # parameters that are not in Agent based have to be declared after:
        self.poolAmountA: int = tokenA_balance
        self.poolAmountB: int = tokenB_balance
        self.hasInitModifierAgent: bool = False

        with open('outdir_csv/logs.txt', 'a') as f:
            f.write('Initial ' +  self.name +  ' tokenA: ' + str(tokenA_balance) + ' ' + 'tokenB: ' + str(tokenB_balance) + '\n')


    def getLiquidityPool(self):
        return (self.poolAmountA, self.poolAmountB)

    def setLiquidityPool(self, _amountA, _amountB):
        self.poolAmountA = _amountA
        self.poolAmountB = _amountB


    def takeStep(self, state):
        self._init(state)

    def _init(self, state):
        if self.hasInitModifierAgent == False:

            # - SWAP -
            # normal distribution
            # np.abs(np.random.normal(loc=0, scale=1, size=101))
            normal_distribution_swap = np.abs(np.random.normal(1.0, 0.5, self.maxTime+2))
            print(normal_distribution_swap)
            # poisson distribution
            poisson_distribution_swap = np.random.poisson(2, self.maxTime+2)
            print(poisson_distribution_swap)
            # binomial
            binomial_distribution_swap = np.random.binomial(n=1, p=0.5, size=[self.maxTime+2])
            print(binomial_distribution_swap)

            # - LIQUIDITY -
            # normal distribution
            normal_distribution_liquidity = np.abs(np.random.normal(1.0, 0.5, self.maxTime+2))
            # print(normal_distribution_liquidity)
            # poisson distribution
            poisson_distribution_liquidity = np.random.poisson(2, self.maxTime+2)
            # print(poisson_distribution_liquidity)
            # binomial
            binomial_distribution_liquidity = np.random.binomial(n=1, p=0.5, size=[self.maxTime+2])
            # print(binomial_distribution_liquidity)

            with open('outdir_csv/logs.txt', 'a') as f:
                f.write(
                    'normal_distribution_swap : ' +  ', '.join(str(e) for e in normal_distribution_swap) + '\n' +
                    'poisson_distribution_swap : ' +  ', '.join(str(e) for e in poisson_distribution_swap) + '\n' +
                    'binomial_distribution_swap : ' +  ', '.join(str(e) for e in binomial_distribution_swap) + '\n' +
                    'normal_distribution_liquidity : ' +  ', '.join(str(e) for e in normal_distribution_liquidity) + '\n' +
                    'poisson_distribution_liquidity : ' +  ', '.join(str(e) for e in poisson_distribution_liquidity ) + '\n' +
                    'binomial_distribution_liquidity : ' +  ', '.join(str(e) for e in binomial_distribution_liquidity) + '\n\n'
                )

            swap_agent_nb = 4 # nb max of this agent type for the simulation
            # add multiple swap agents
            for i in range(swap_agent_nb):
                swap_agent = SwapAgent.SwapAgent(
                    name= "swap_"+str(i),
                    getLiquidityPool= self.getLiquidityPool,
                    setLiquidityPool= self.setLiquidityPool,
                    uniswapV2Router= uniswapV2Router,
                    tokenA= tokenA,
                    tokenB= tokenB,
                    lpToken= self.lpToken,
                    normal_distribution= normal_distribution_swap,
                    binomial_distribution= binomial_distribution_swap,
                    poisson_distribution= poisson_distribution_swap
                )
                state.addAgent(swap_agent) # add this agent to the simulation

            liquidity_agent_nb = 1
            for i in range(liquidity_agent_nb):
                liquidity_agent = LiquidityAgent.LiquidityAgent(
                    name= "liquidity_"+str(i),
                    getLiquidityPool= self.getLiquidityPool,
                    setLiquidityPool= self.setLiquidityPool,
                    uniswapV2Router= uniswapV2Router,
                    tokenA= tokenA,
                    tokenB= tokenB,
                    lpToken= self.lpToken,
                    normal_distribution= normal_distribution_liquidity,
                    binomial_distribution= binomial_distribution_liquidity,
                    poisson_distribution= poisson_distribution_liquidity
                )
                state.addAgent(liquidity_agent)

            # add them only at beginning of the simulation
            self.hasInitModifierAgent = True