import random
from typing import List

from enforce_typing import enforce_types

from engine import AgentBase
from util import globaltokens
from util.base18 import toBase18
from util import constants

from util.constants import BROWNIE_PROJECT080

import json
from brownie import accounts, Contract
from brownie import web3

@enforce_types
class SwapAgent(AgentBase.AgentBaseEvm):

    def __init__(self, name: str, getLiquidityPool, setLiquidityPool, uniswapV2Router, tokenA, tokenB, lpToken, normal_distribution, binomial_distribution, poisson_distribution):
        super().__init__(name)
        # parameters that are not in Agent based have to be declared after:
        self.id = int(name[-1])
        self.getLiquidityPool = getLiquidityPool
        self.setLiquidityPool= setLiquidityPool
        self.uniswapV2Router = uniswapV2Router
        self.tokenA = tokenA
        self.tokenB = tokenB
        self.lpToken = lpToken
        # normal distribution amount
        self.normal_distribution = normal_distribution
        self.binomial_distribution = binomial_distribution
        self.poisson_distribution = poisson_distribution
        self.i = 0 # day
        # transfer tokens on initialization
        unlocked_metamask = accounts.at('0x7bBfecDCF7d0E7e5aA5fffA4593c26571824CB87', force=True)
        unlocked_metamask.transfer(self._wallet.account.address, "0.01 ether") # send eth to the agent
        # send token to the agent
        tokenA.transfer(self._wallet.account.address, 0.003*10**18, {'from': unlocked_metamask}) 
        tokenB.transfer(self._wallet.account.address, 0.001*10**18, {'from': unlocked_metamask}) 
        # Take the first action now ! (loi poisson)
        if(self.poisson_distribution[self.i] >= self.id):
            self._swapTokens()

    def takeStep(self, state):
        self.i = self.i + 1

        if(self.poisson_distribution[self.i] >= self.id):
            self._swapTokens()
    
    # swapExactTokensForTokens
    def _swapTokens(self):
        # The minimum amount of output tokens that must be received for the transaction not to revert.
        amountOutMin = 1
        # Recipient of the output tokens.
        to = self._wallet.account.address
        # deadline
        current_block = web3.eth.get_block('latest')
        current_timestamp = current_block['timestamp']
        deadline = current_timestamp + 10
        # ratio tokenA/b value
        tokenA_balance = self.tokenA.balanceOf(self.lpToken.address)
        tokenB_balance = self.tokenB.balanceOf(self.lpToken.address)
        _max = max(tokenA_balance, tokenB_balance)
        _min = min(tokenA_balance, tokenB_balance)
        ratio = _max / _min
        # constant swap value 
        c = 0.0000001*10**18

        # swap direction
        if(self.binomial_distribution[self.i] == 1):
            # The amount of input tokens to send | constant * normal distribution value
            if(tokenA_balance > tokenB_balance):
                amountIn = c * self.normal_distribution[self.i] * ratio
            else:
                amountIn = c * self.normal_distribution[self.i]
            # An array of token addresses. path.length must be >= 2. Pools for each consecutive pair of addresses must exist and have liquidity.
            path = [self.tokenA.address, self.tokenB.address]
            # allowance uniswap router
            self.tokenA.approve(self.uniswapV2Router.address, amountIn, {'from': to})
        else:
            if(tokenA_balance > tokenB_balance):
                amountIn = c * self.normal_distribution[self.i]
            else:
                amountIn = c * self.normal_distribution[self.i] * ratio
            path = [self.tokenB.address, self.tokenA.address]
            self.tokenB.approve(self.uniswapV2Router.address, amountIn, {'from': to})

        # print('BEFORE :')
        # print(self.tokenA.balanceOf(to)/10**18 , 'UNI')
        # print(self.tokenB.balanceOf(to)/10**18 , 'WETH')

        tx = self.uniswapV2Router.swapExactTokensForTokens(amountIn, amountOutMin, path, to, deadline, {"from": to})
  
        # Wait for the transaction to be confirmed
        # tx_receipt = web3.eth.wait_for_transaction_receipt(tx.txid)
        
        # print('AFTER :')
        # print(self.tokenA.balanceOf(to)/10**18 , 'UNI')
        # print(self.tokenB.balanceOf(to)/10**18 , 'WETH')

        # Uniswap liquidity pool
        tokenA_balance = self.tokenA.balanceOf(self.lpToken.address)
        tokenB_balance = self.tokenB.balanceOf(self.lpToken.address)

        self.setLiquidityPool(tokenA_balance, tokenB_balance)

        with open('outdir_csv/logs.txt', 'a') as f:
            f.write('Swap ' +  self.name +  ' tokenA: ' + str(tokenA_balance) + ' ' + 'tokenB: ' + str(tokenB_balance) + '\n')