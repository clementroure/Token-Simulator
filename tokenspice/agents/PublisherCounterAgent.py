import random
from typing import List

from enforce_typing import enforce_types

from engine import AgentBase
from util import globaltokens
from util.base18 import toBase18
from util import constants

from util.constants import BROWNIE_PROJECT080

from agents import ModifierCounterAgent, WalletCounterAgent

@enforce_types
class PublisherCounterAgent(AgentBase.AgentBaseNoEvm):

    def __init__(self, name: str, USD: float, OCEAN: float, counter: int, address: str):
        super().__init__(name, USD, OCEAN)
        # parameters that are not in Agent based have to be declared after:
        self.counter: int = counter
        self.address: str = address
        self.hasInitModifierAgent: bool = False


    def getCounter(self):
        return self.counter

    def setCounter(self, value):
        self.counter = value
        # print(self.counter)


    def takeStep(self, state):
        self._init(state)

    def _init(self, state):

        if self.hasInitModifierAgent == False:
            agent_nb = 3
            # add multiple agents
            for i in range(agent_nb):
                modifier_agent = ModifierCounterAgent.ModifierCounterAgent(
                    name= "inc"+str(i),
                    USD= 0.0,
                    OCEAN= 5.0,
                    address= self.address, #counter contract address
                    getCounter= self.getCounter,
                    setCounter= self.setCounter
                )
                state.addAgent(modifier_agent) # add this agent to the simulation

            # add test agent wallet eth
            wallet_agent = WalletCounterAgent.WalletCounterAgent(
                name= "wallet1",
                USD= 0.0,
                OCEAN= 5.0,
                contractAddress= self.address, #counter contract address
            )
            state.addAgent(wallet_agent) # add this agent to the simulation

            # add them only at beginning of the simulation
            self.hasInitModifierAgent = True