import random
from typing import List

from enforce_typing import enforce_types

from engine import AgentBase
from util import globaltokens
from util.base18 import toBase18
from util import constants

from util.constants import BROWNIE_PROJECT080

# test abi call (UniswapRouterV2)
import json
from brownie import accounts, Contract
from brownie import web3
# normal law
from scipy.stats import truncnorm
# poisson
import numpy as np

# Uniswap Router
with open("abi/UniswapV2Router02.json") as file:
    abi = json.load(file)
# https://etherscan.io/address/0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D#code
uniswapV2Router_address = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D"
contract = Contract.from_abi("UniswapV2Router02", uniswapV2Router_address, abi)

# Uniswap Factory
# Uniswap
with open("abi/UniswapV2Factory.json") as file:
    abi = json.load(file)
# https://etherscan.io/address/0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f#code
uniswapV2Factory_address = "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f"
factoryContract = Contract.from_abi("UniswapV2Factory", uniswapV2Factory_address, abi)

# TokenA
with open("abi/UNI.json") as file:
    abi = json.load(file)
# https://goerli.etherscan.io/address/0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984
tokenA_address = "0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984"
tokenA = Contract.from_abi("UNI", tokenA_address, abi)

# TokenB
with open("abi/WETH.json") as file:
    abi = json.load(file)
# https://goerli.etherscan.io/address/0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6#code
tokenB_address = "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6"
tokenB = Contract.from_abi("WETH", tokenB_address, abi)

# USDC goerli
with open("abi/USDC.json") as file:
    abi = json.load(file)
USDC_address = "0x07865c6e87b9f70255377e024ace6630c1eaa37f"
USDC = Contract.from_abi("USDC", USDC_address, abi)

@enforce_types
class WalletCounterAgent(AgentBase.AgentBaseEvm):

    def __init__(self, name: str, USD: float, OCEAN: float, contractAddress: str):
        super().__init__(name, USD, OCEAN)
        # parameters that are not in Agent based have to be declared after:
        self.contractAddress: str = contractAddress
        # transfer tokens on initialization
        unlocked_metamask = accounts.at('0x7bBfecDCF7d0E7e5aA5fffA4593c26571824CB87', force=True)
        unlocked_metamask.transfer(self._wallet.account.address, "0.1 ether") # send eth to the agent
        # send token to the agent
        tokenA.transfer(self._wallet.account.address, 0.003*10**18, {'from': unlocked_metamask}) 
        tokenB.transfer(self._wallet.account.address, 0.001*10**18, {'from': unlocked_metamask}) 
        USDC.transfer(self._wallet.account.address, 100*10**6, {'from': unlocked_metamask}) 

        # normal law repartition
        X = self.get_truncated_normal(5, 1, 1, 10)
        print(X.rvs(10))
        # random_numbers = np.abs(np.random.normal(loc=0, scale=1, size=10))
        # print(random_numbers)

        # poisson distribution
        s = np.random.poisson(5, 10)
        print(s)

        # brownie.exceptions.VirtualMachineError: revert = no more token in metamask wallet

    def takeStep(self, state):
        # rnd = random.randint(0, 1)
        # if rnd == 0:
        #    self._payMe(state)
        # else:
        #    self._payMe(state)
        #
        # test methods
        # self._addLiquidityETH()
        self._addLiquidity()
        # self._swapTokens()

    def _payMe(self, state):
        counterContract = BROWNIE_PROJECT080.Counter.at(self.contractAddress)
        counterContract.payMe(self._wallet.account.address)
        
        # print(counterContract.getBalance())
        # print(self._wallet.account.balance() / 10**18)

    def get_truncated_normal(self, mean, sd, low, upp):
        return truncnorm(
            (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)

    
    def _removeLiquidity(self):
        # The minimum amount of tokenA that must be received for the transaction not to revert.
        amountAMin = 1
        # The minimum amount of tokenB that must be received for the transaction not to revert.
        amountBMin = 1

        to = self._wallet.account.address
        
        current_block = web3.eth.get_block('latest')
        current_timestamp = current_block['timestamp']
        deadline = current_timestamp + 10

        # liquidity token address
        tokenPair = factoryContract.getPair.call(tokenA_address, tokenB_address)
        print(tokenPair)

        # get ERC20 abi
        with open("abi/LPToken.json") as file:
            erc20_abi = json.load(file)
        LPToken = Contract.from_abi("LP Token", address=tokenPair, abi=erc20_abi)

        # get amount of tokens in liquidity pool
        tokenA_balance = tokenA.balanceOf(tokenPair)
        tokenB_balance = tokenB.balanceOf(tokenPair)
        print()
        print('- UNISWAP POOL -')
        print('UNI balance: ', tokenA_balance / 10**18)
        print('WETH balance: ', tokenB_balance / 10**18)
        print()
        # LpToken balance: addliquidity a token and b token = x lptoken. So, multiply by c, to get a/b token addded = a/b total balance. Multiply x lptoken by c to get the lptoken total supply
        # c =  tokenA_balance / tokenA amount addded
        # totalLPsupply = lpToken added * c

        # check liquidity amount (agent balances)
        print('- AGENT WALLET -')
        print('UNI: ', tokenA.balanceOf(to) / 10**18)
        print('WETH: ', tokenB.balanceOf(to) / 10**18)
        liquidity = LPToken.balanceOf(to)
        print("LP balance: ", liquidity / 10**18)
        print()

        # Allowance -> uniswap can take agent's liquidity tokens
        LPToken.approve(uniswapV2Router_address, liquidity/2, {'from': to})
        
        tx = contract.removeLiquidity(tokenA, tokenB, liquidity/2, amountAMin, amountBMin, to, deadline, {"from": to})   
        tx.wait(1)

    def _addLiquidity(self):
        # The amount of tokenA to add as liquidity if the B/A price is <= amountBDesired/amountADesired (A depreciates).
        amountADesired = 0.0004*10**18
        # The amount of tokenB to add as liquidity if the A/B price is <= amountADesired/amountBDesired (B depreciates).
        amountBDesired = 0.0001*10**18

        # Bounds the extent to which the B/A price can go up before the transaction reverts. Must be <= amountADesired.
        amountAMin = 1
        # Bounds the extent to which the A/B price can go up before the transaction reverts. Must be <= amountBDesired.
        amountBMin = 1

        # Recipient of the liquidity tokens
        to = self._wallet.account.address

        # Unix timestamp after which the transaction will revert
        current_block = web3.eth.get_block('latest')
        current_timestamp = current_block['timestamp']
        deadline = current_timestamp + 3

        # Allowance -> uniswap can take agent's tokens
        tokenA.approve(uniswapV2Router_address, amountADesired, {'from': to})
        tokenB.approve(uniswapV2Router_address, amountBDesired, {'from': to})

        # WARNING: use .call() to get teh returned value !   # remove .call() and put again if cant mint liquidity
        tx = contract.addLiquidity(tokenA, tokenB, amountADesired, amountBDesired, amountAMin, amountBMin, to, deadline, {"from": to}) 
        # print(tx.txid)

        # Wait for the transaction to be confirmed
        tx_receipt = web3.eth.wait_for_transaction_receipt(tx.txid)

        # Get the return value from the transaction receipt
        returned_value = tx_receipt['contractAddress']

        # # Print the returned value
        # print(returned_value)
        
        # returned_value = contract.addLiquidity.call(tokenA, tokenB, amountADesired, amountBDesired, amountAMin, amountBMin, to, deadline, {"from": to}) 
        # print('amountA:', returned_value[0], 'amountB:', returned_value[1], 'Liquidity:', returned_value[2])

        self._removeLiquidity()

    def _addLiquidityETH(self):
        amountTokenDesired = 10*10**6 # put 10 USDC in pool
        amountTokenMin = 0
        amountETHMin = 0
        # lp receiver
        to = self._wallet.account.address
        # deadline
        current_block = web3.eth.get_block('latest')
        current_timestamp = current_block['timestamp']
        deadline = current_timestamp + 3
        # Allowance -> uniswap can take agent's tokens
        USDC.approve(uniswapV2Router_address, amountTokenDesired, {'from': to})
        # test before call method
        print('ETH: ', self._wallet.account.balance() / 10**18)
        print('USDC: ', (USDC.balanceOf(to))/10**6)
        # call uniswap method
        tx = contract.addLiquidityETH.call(USDC_address, amountTokenDesired, amountTokenMin, amountETHMin, to, deadline, {"from": to, "value": "0.01 ether"})
        print(tx.txid)

        # Wait for the transaction to be confirmed
        tx_receipt = web3.eth.wait_for_transaction_receipt(tx.txid)

        # Get the return value from the transaction receipt
        returned_value = tx_receipt['contractAddress']

        # Print the returned value
        print(returned_value)

        self._removeLiquidityETH()

    def _removeLiquidityETH(self):
        amountTokenMin = 0
        amountETHMin = 0

        to = self._wallet.account.address
        
        current_block = web3.eth.get_block('latest')
        current_timestamp = current_block['timestamp']
        deadline = current_timestamp + 10

        # liquidity token address
        tokenPair = '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc'

        # get ERC20 abi
        with open("abi/LPToken.json") as file:
            erc20_abi = json.load(file)
        LPToken = Contract.from_abi("LP Token", address=tokenPair, abi=erc20_abi)

        # check liquidity amount
        liquidity = LPToken.balanceOf(to)
        print(liquidity)

        # Allowance -> uniswap can take agent's liquidity tokens
        LPToken.approve(uniswapV2Router_address, liquidity, {'from': to})

        tx = contract.removeLiquidityETH(USDC_address, liquidity, amountTokenMin, amountETHMin, to, deadline, {"from": to})   
    
    # swapExactTokensForTokens
    def _swapTokens(self):
        # The amount of input tokens to send.
        amountIn = 0.0001*10**18
        # The minimum amount of output tokens that must be received for the transaction not to revert.
        amountOutMin = 1
        # An array of token addresses. path.length must be >= 2. Pools for each consecutive pair of addresses must exist and have liquidity.
        path = [tokenA_address, tokenB_address]
        # Recipient of the output tokens.
        to = self._wallet.account.address

        current_block = web3.eth.get_block('latest')
        current_timestamp = current_block['timestamp']
        deadline = current_timestamp + 300

        tokenA.approve(uniswapV2Router_address, amountIn, {'from': to})

        print('BEFORE :')
        print(tokenA.balanceOf(to)/10**18 , 'UNI')
        print(tokenB.balanceOf(to)/10**18 , 'WETH')

        tx = contract.swapExactTokensForTokens(amountIn, amountOutMin, path, to, deadline, {"from": to})
  
        # Wait for the transaction to be confirmed
        tx_receipt = web3.eth.wait_for_transaction_receipt(tx.txid)
        
        print('AFTER :')
        print(tokenA.balanceOf(to)/10**18 , 'UNI')
        print(tokenB.balanceOf(to)/10**18 , 'WETH')
