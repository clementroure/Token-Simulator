from typing import List

from enforce_typing import enforce_types

from agents import PublisherUniswapV2Agent
from engine import KPIsBase, SimStateBase, SimStrategyBase
from util.constants import S_PER_HOUR, S_PER_DAY
from util.plotutil import YParam, arrayToFloatList, LINEAR, MULT1, COUNT, DOLLAR
from util.strutil import prettyBigNum

from util.constants import BROWNIE_PROJECT080, GOD_ACCOUNT

@enforce_types
class SimStrategy(SimStrategyBase.SimStrategyBase):
    def __init__(self):
        super().__init__()

        # ==baseline
        self.setTimeStep(S_PER_DAY)
        self.setMaxTime(99, "days")
        # self.setLogInterval(S_PER_DAY)

@enforce_types
class SimState(SimStateBase.SimStateBase):
    def __init__(self, ss=None):
        assert ss is None
        super().__init__(ss)
        # ss is defined in this netlist module
        self.ss = SimStrategy()

        # wire up the circuit
        pub_agent = PublisherUniswapV2Agent.PublisherUniswapV2Agent(
            name= "pub"
        )
        self.agents[pub_agent.name] = pub_agent

        # kpis is defined in this netlist module
        self.kpis = KPIs(self.ss.time_step)

@enforce_types
class KPIs(KPIsBase.KPIsBase):
    pass

@enforce_types
def netlist_createLogData(state):
    """SimEngine constructor uses this"""
    s = []  # for console logging
    dataheader = []  # for csv logging: list of string
    datarow = []  # for csv logging: list of float

    # SimEngine already logs: Tick, Second, Min, Hour, Day, Month, Year
    # So we log other things...

    publisher = state.getAgent("pub")
    # s += [f"; Liquidity Pool={(publisher.getLiquidityPool()[0] / 10**18, publisher.getLiquidityPool()[1] / 10**18)}"]
    # dataheader += ["Liquidity Pool"]
    # datarow += [(publisher.getLiquidityPool()[0] / 10**18, publisher.getLiquidityPool()[1] / 10**18)]
    s += [f"; amountA={publisher.getLiquidityPool()[0] / 10**18}"]
    dataheader += ["amountA"]
    datarow += [publisher.getLiquidityPool()[0] / 10**18]
    #
    s += [f"; amountB={publisher.getLiquidityPool()[1] / 10**18}"]
    dataheader += ["amountB"]
    datarow += [publisher.getLiquidityPool()[1] / 10**18]

    # done
    return s, dataheader, datarow


@enforce_types
def netlist_plotInstructions(header: List[str], values):
    """
    Describe how to plot the information.
    tsp.do_plot() uses this.

    :param: header: List[str] holding 'Tick', 'Second', ...
    :param: values: 2d array of float [tick_i, valuetype_i]
    :return: x_label: str -- e.g. "Day", "Month", "Year"
    :return: x: List[float] -- x-axis info on how to plot
    :return: y_params: List[YParam] -- y-axis info on how to plot
    """
    x_label = "Year"
    x = arrayToFloatList(values[:, header.index(x_label)])

    y_params = [
        YParam(
            ['Liquidity pool'],
            ['Liquidity pool'],
            "Liquidity pool over time",
            LINEAR,
            MULT1,
            DOLLAR,
        )
    ]

    return (x_label, x, y_params)
