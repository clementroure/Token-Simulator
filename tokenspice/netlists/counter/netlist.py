from typing import List

from enforce_typing import enforce_types

from agents import PublisherUniswapV2Agent
from engine import KPIsBase, SimStateBase, SimStrategyBase
from util.constants import S_PER_HOUR, S_PER_DAY
from util.plotutil import YParam, arrayToFloatList, LINEAR, MULT1, COUNT, DOLLAR
from util.strutil import prettyBigNum

from util.constants import BROWNIE_PROJECT080, GOD_ACCOUNT

@enforce_types
class SimStrategy(SimStrategyBase.SimStrategyBase):
    def __init__(self):
        super().__init__()

        # ==baseline
        self.setTimeStep(S_PER_DAY)
        self.setMaxTime(10, "days")
        # self.setLogInterval(S_PER_DAY)

        # ==attributes specific to this netlist

        self.count = 1

@enforce_types
class SimState(SimStateBase.SimStateBase):
    def __init__(self, ss=None):
        assert ss is None
        super().__init__(ss)

        # ss is defined in this netlist module
        self.ss = SimStrategy()

        # Deplot counter contract here ?
        counterContract = BROWNIE_PROJECT080.Counter.deploy(
            {"from": GOD_ACCOUNT}
        )
        GOD_ACCOUNT.transfer(counterContract.address, "1 ether") # send eth to the contract

        # wire up the circuit
        pub_agent = PublisherUniswapV2Agent.PublisherUniswapV2Agent(
            name= "pub1",
            USD= 0.0,
            OCEAN= 5.0,
            # address= counterContract.address
        )
        self.agents[pub_agent.name] = pub_agent

        # kpis is defined in this netlist module
        self.kpis = KPIs(self.ss.time_step)

@enforce_types
class KPIs(KPIsBase.KPIsBase):
    pass

@enforce_types
def netlist_createLogData(state):
    """SimEngine constructor uses this"""
    s = []  # for console logging
    dataheader = []  # for csv logging: list of string
    datarow = []  # for csv logging: list of float

    # SimEngine already logs: Tick, Second, Min, Hour, Day, Month, Year
    # So we log other things...

    publisher = state.getAgent("pub1")
    s += [f"; counter={(publisher.getLiquidityPool()[0] / 10**18, publisher.getLiquidityPool()[1] / 10**18)}"]
    dataheader += ["counter"]
    datarow += [(publisher.getLiquidityPool()[0] / 10**18, publisher.getLiquidityPool()[1] / 10**18)]

    # crash sans le try car:
    # au 1er tour, wallet agent and incr agents are not implemented yet
    # try:
    #     wallet = state.getAgent("wallet1")
    #     s += [f"; eth={wallet._wallet.account.balance()}"]
    #     dataheader += ["eth"]
    #     datarow += [wallet._wallet.account.balance() / 10**18]
    # except:
    #     dataheader += ["eth"]
    #     datarow += [0]
    #     print("An exception occurred")

    # pool_agents = state.agents.filterToPool()
    # n_pools = len(pool_agents)
    # s += [f"; # pools={n_pools}"]
    # dataheader += ["n_pools"]
    # datarow += [n_pools]

    # done
    return s, dataheader, datarow


@enforce_types
def netlist_plotInstructions(header: List[str], values):
    """
    Describe how to plot the information.
    tsp.do_plot() uses this.

    :param: header: List[str] holding 'Tick', 'Second', ...
    :param: values: 2d array of float [tick_i, valuetype_i]
    :return: x_label: str -- e.g. "Day", "Month", "Year"
    :return: x: List[float] -- x-axis info on how to plot
    :return: y_params: List[YParam] -- y-axis info on how to plot
    """
    x_label = "Year"
    x = arrayToFloatList(values[:, header.index(x_label)])

    y_params = [
        YParam(
            ['counter'],
            ['counter'],
            "Counter over time",
            LINEAR,
            MULT1,
            DOLLAR,
        )
    ]

    return (x_label, x, y_params)
