"""
Main classes in this module:
-AgentBaseAbstract - abstract interface
-AgentBaseNoEvm - hold AgentWalletNoEvm
-AgentBaseEvm - hold AgentWalletEvm
-Sub-class AgentBase{NoEvm,Evm} for specific agents (buyers, publishers, ..)
"""

from abc import ABC, abstractmethod
import logging

from enforce_typing import enforce_types

from engine.AgentWallet import AgentWalletAbstract, AgentWalletEvm, AgentWalletNoEvm
from util.constants import SAFETY
from util.strutil import StrMixin

log = logging.getLogger("baseagent")

@enforce_types
class AgentBaseAbstract(ABC):
    def __init__(self, name: str):
        self.name = name
        self._wallet: AgentWalletAbstract

    @abstractmethod
    def takeStep(self, state):  # this is where the Agent does *work*
        pass

#L'agent généré a partir de cette classe n'a pas de callet
@enforce_types
class AgentBaseNoEvm(StrMixin, AgentBaseAbstract):
    def __init__(self, name: str):
        AgentBaseAbstract.__init__(self, name)
        self._wallet: AgentWalletNoEvm = AgentWalletNoEvm()

#L'agent généré a partir de cette classe a un wallet
@enforce_types
class AgentBaseEvm(StrMixin, AgentBaseAbstract):
    def __init__(self, name: str):
        AgentBaseAbstract.__init__(self, name)
        self._wallet: AgentWalletEvm = AgentWalletEvm()

    @property
    def address(self) -> str:
        return self._wallet.address

    @property
    def account(self) -> str:
        return self._wallet.account

    # datatoken and pool-related
    def DT(self, dt) -> float:
        return self._wallet.DT(dt)

    def BPT(self, pool) -> float:
        return self._wallet.BPT(pool)