import brownie
from pytest import approx

from util.constants import BROWNIE_PROJECT080, GOD_ACCOUNT

accounts = brownie.network.accounts
account0, account1 = (
    accounts[0],
    accounts[1],
)
address0, address1 = account0.address, account1.address

# deploy constructor
counter = BROWNIE_PROJECT080.Counter.deploy(
    {"from": account0},
)

def test_increment():

    counter.incrementCounter()
    assert counter.getCount() == 1

def test_decrement():
    counter.decrementCounter()
    counter.decrementCounter()
    assert counter.getCount() == -1
