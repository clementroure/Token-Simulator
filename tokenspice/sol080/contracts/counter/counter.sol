// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

contract Counter {
    int private count = 0;
    function incrementCounter() public {
        count += 1;
    }
    function decrementCounter() public {
        count -= 1;
    }

    function getCount() public view returns (int) {
        return count;
    }

    function payMe(address _to) public payable {
        // uint256 nb = random() % 2;
        uint256 amountToSend = 0.01 ether;
        
        (bool sent,) = payable(_to).call{value: amountToSend}("");
        require(sent, "Failed to send Ether");
    }

    // Pseudo-random Number
    function random() internal view returns (uint256) {
        return uint256(keccak256(abi.encodePacked(
        tx.origin,
        blockhash(block.number - 1),
        block.timestamp
        )));
    }

    function getBalance() public view returns(uint){
        return address(this).balance;
    }

    receive() external payable {}
}