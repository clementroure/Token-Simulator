from flask import Flask, request
from flask_cors import CORS
import subprocess
from subprocess import Popen, run

app = Flask(__name__)
CORS(app)

# API routes base
@app.route("/")
def main():
    return "Server running on port 5000"

@app.route("/restart_ganache")
def restartGanache():
    ganache_process.kill()
    ganache_process = subprocess.Popen('ganache.py', shell=True)
    return "Ganache restarted"

@app.route("/stop_simulation")
def stopSimulation():
    simulation_process.kill()
    return "Simulation stopped"

# API routes Simulation type
@app.route("/uniswapv2", methods=['GET', 'POST'])
def uniswapv2():
    if request.method == 'POST':
        test = request.json['test']
        print('TEST ', test)
        
        subprocess.run('rm -rf outdir_csv', shell=True)
        simulation_process = subprocess.run('python tsp run netlists/uniswapv2/netlist.py outdir_csv', shell=True)

        return "Uniswap V2"
    else:
        return "Error: This is a POST method"

if __name__ == "__main__":
    ganache_process = subprocess.Popen('ganache.py', shell=True)
    app.run(debug=False)